# depth.fd : fast data depth computation for random functions #
## Stanislav Nagy ##

Computation of various types of **depth for functional data** for both univariate (*R*-valued), and bivariate (*R^2*-valued) curves. The depths designed for bivariate functional data may be used for the computation of depths of random functions and their derivatives, incorporating the information about the shapes of the curves. For an overview of the depths implemented in the package see the Supplementary Material provided with *Nagy et al. (2016)*. 

For functional data whose values are observed contaminated with additive noise, preprocessing of the curves by means of **fast kernel smoothing** is available. For details see *Nagy and Ferraty (2016)*. 

The historical population-by-country data example of functional data is included.

The package is provided with the complete source codes of the R interface, and the Fortran 77 procedures.

To install the package *depth.fd* from the attached file "depth.fd_1.0.tar.gz", in Windows first make sure that you have Rtools installed on your computer, see 
             https://cran.r-project.org/bin/windows/Rtools/.
Then run the following code


```
#!R

install.packages("https://bitbucket.org/StanislavNagy/depth.fd_public/raw/501b411f4f363141e417938aa9b4f810fdb9c897/depth.fd_1.0.tar.gz",type="source")
library(depth.fd)
help(package=depth.fd)
help(FKS)
help(depth.fd1)
```


The key in the link above (after raw/) should be modified to link to the newest version of the depth.fd tar.gz file. Alternatively, download the latest version of the tar.gz file to the R working directory in your computer (see getwd() in R) and run

```
#!R

install.packages("depth.fd_1.0.tar.gz", type="source", repos=NULL)
```

## Installation from source ##

To build the "depth.fd" package from source in R:

1. in Windows, make sure that you have Rtools installed on your computer, see 
             https://cran.r-project.org/bin/windows/Rtools/
2. place all the necessary source files into the folder (R working directory, see getwd())/package_depth.fd:
    * depth.fd.f
    * depth.fd.R
    * depth.fd-package.Rd
    * DESCRIPTION
    * population.rda
    * population2010.rda
3. if not installed, install R packages "roxygen2" and "devtools" available from the CRAN repository
4. run the following code in R
      (**note:** some of the commands below may produce warnings, as they are intended to purge R of any earlier copies of the package, if applicable. Once run, to re-install the package from the source run the code again without the first three lines. Due to a known bug in R before re-installing the package from source, R should be restarted. Otherwise, the help files are not loaded correctly. Tested in Windows 7 and R x64 3.3.0)
            
```
#!R
            install.packages("roxygen2")
            install.packages("devtools")   # install packages
            setwd("package_depth.fd")      # set the working directory
            #
            unlink("depth.fd",recursive=TRUE)
            detach("package:depth.fd", unload=TRUE)
            remove.packages("depth.fd")
            library(roxygen2)
            library(devtools)
            package.skeleton("depth.fd",code_files="depth.fd.R")
            roxygenize("depth.fd")
            dir.create("depth.fd/src")
            file.copy("depth.fd.f","depth.fd/src/depth.fd.f")
            dir.create("depth.fd/data")
            file.copy("population.rda","depth.fd/data/population.rda")
            file.copy("population2010.rda","depth.fd/data/population2010.rda")
            file.copy("DESCRIPTION","depth.fd/DESCRIPTION",overwrite=TRUE)
            file.copy("depth.fd-package.Rd","depth.fd/man/depth.fd-package.Rd",overwrite=TRUE)
            build("depth.fd")
            install.packages("depth.fd_1.0.tar.gz", type="source", repos=NULL)
            library(depth.fd);help(depth.fd)
```

In case of questions contact 

***Stanislav Nagy***

*stanislav.nagy@kuleuven.be*

### References ###

Nagy, S., Gijbels, I. and Hlubinka, D. (2016), Depth-based recognition of shape outlying functions, *Under review.*

Nagy, S. and Ferraty, F. (2016), Data depth for measurable noisy random functions, *Under review.*